from django.test import TestCase
from django.test import Client
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser, User

class RouteTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com')
        user.set_password('password')
        user.save()

    def test_home(self):
        client = Client()
        response = client.get('')
        self.assertEqual(response.status_code, 200) #check the return valur which is http response code
    
    def test_signup(self):
        client = Client()
        response = client.post('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_room(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.get('/rooms/')
        self.assertEqual(response.status_code, 302)

    def test_reservations(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.get('/reservations/')
        self.assertEqual(response.status_code, 302)

    def test_roomdetail(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.get('/room/101')
        self.assertEqual(response.status_code, 302)

    def test_reservation(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.get('/reservation/1')
        self.assertEqual(response.status_code, 403)

    def test_customer(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.get('/customer/1')
        self.assertEqual(response.status_code, 403)
    
    def test_staff(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.get('/staff/1')
        self.assertEqual(response.status_code, 302)
    
    def test_profile(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.get('/profile/')
        self.assertEqual(response.status_code, 404)

    def test_guests(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.get('/guests/')
        self.assertEqual(response.status_code, 302)

    def test_reserve(self):
        client = Client()
        login = client.login(username='john', password='password')
        u = auth.authenticate(username='john', password='password')
        u.save()
        response = client.post('/reserve/', {'first_name':'john','middle_name':'b'
        ,'email':'a@a.a', 'last_name':'b', 'contact_no':'0', 
        'address':'a', 'no_of_children':1, 'no_of_adults':2,
        'expected_arrival_date_time':'2018-11-10',
        'expected_departure_date_time':'2018-11-10',
        'rooms':101})
        self.assertEqual(response.status_code, 403)